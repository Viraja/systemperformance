# Batch reactor feature based regression

Scripts to reproduce the analyses of batch reactors signals reported in the article [Article](link)

## Authors

* **Mariane Schneider**

## License

This project is licensed under the GPLv3+ license - see the [LICENSE](LICENSE) file for details

## Repository structure
The folder structure of the repository is:

    .
    ├── data
    ├── doc
    └── python

The `data` folder is meant to store raw data and the output of scripts.
It is actually a placeholder, no data should be added to the repository.
The data was used for the corresponding publication can be found here:
 [![DOI](https://zenodo.org/badge/DOI/10.25678/000194.svg)](https://doi.org/10.25678/000194)


The `doc` folder contains documents used during the development of this package.
They do not contain actual documentation; to get documentation please refer to the [article](Link).

The `python` folder contains scripts implementing the analysis of the data using the features from the [](https://gitlab.com/Viraja/systemperformance) module.

## How to create the figures from the article?
The scripts can be found in the python folder. The data needs to be downloaded first and saved into the data folder.
* s_O2_ROCcurve.py creates the DO_Tables.pkl and DO_ROC.pkl needed for s_fig_TFtable DO.
