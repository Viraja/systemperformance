# Data folder

This folder is for the raw data used to test the features for regression.
Please do not add raw data to the repository.

Data created by the scripts in the repository are stored here as well.
After you run some of the scripts you will get some files here.
Please do not add them to the repository.

The full raw dataset will be shared in the near future at [![DOI](https://zenodo.org/badge/DOI/10.25678/000194.svg)](https://doi.org/10.25678/000194).
