# vim: set tabstop=4
# inputdict.py
#!/usr/bin/env python3
""" These are all model parameters which you only have to change here.
The Path should only be changed here as well.
to do: check all the input parameters."""

# Copyright (C) 2015 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 26.06.2015


## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
import sys

pythonScriptPath = "C:/marianes/04 Modeling/Python/PerformanceModel - fertig/Plots/20170905 - Montecarlos ICUD/"
#pythonScriptPath = "C:/Users/Marianes/polybox/Model/PerformanceModel"  # Path where SNIP python files are stored
sys.path.append(pythonScriptPath)


## ------------- 1. Input Parameters -----------------
## ---------------------------------------------------
## timeMax [days]= Time of simulation = 30 years =              30*365 days
## nbOfUnits [#]= Total number of wastewater treatment units =  1000 units
## construction [days] = Time over which all the units were constructed =
##                                                              10*365 days
##
## failure (weibull):
## k [-] = shape =                                              2
## lambda [days] = scale =                                      412 days
##
## repair (weibull):
## k [-] = shape =                                              3
## lambda [days] = scale =                                      15.7 days
##
## insp_int [days] = inspection interval =                      every 365 days

inputdata = {
    'timeMax': 10*365,              # simulation time [days]
    'failureInterval': 65.0,        # failure intervall of sensor [days]
    'nbOfUnits': 1000,              # number of units [#]
    'constructionVariation': 0,     # if 0 = all from beginning [days]

#    'shape_repair': 3.0,
#    'scale_repair': 6.7,

    'shape_fail': 2.0,
    'scale_fail': 412.0,

    'sensorFailure': 'yes',         # 'no' if no sensor failure, else 'yes'

    'failureRateSensor': 1.0,       # 0.0 test repair, 1.0 for inspection
    'perfIfTrue': 0.8,              # performance/reduction of a working unit
    'perfIfFalse': 0.0,             # performance/reduction of a failed unit

    ## ------------- 2. Sensor ---------------------------
    ## ---------------------------------------------------
    'sensorIs': 1,                  # 0: no sensor
                                    # 1: sensor

    ## ------------- 3. Personnel ------------------------
    ## ---------------------------------------------------
    'nbOfInspector': 1000,
    'nbOfTechnician': 1000,         # high number if not limited by workers
    'nbOfIHrPerDay': 8,
    'nbOfHrPerDay': 8,
    'inspectionInterval': 365*2,
    'weekend': 'off',               # on or off depending on if considered

    ## ------------- 4. Repair ---------------------------
    ## ---------------------------------------------------
    'timeDrive': 1,
    'timeRepair': 0.5,

    ## ------------- 5. Unit Types Variable definition ---
    ## ---------------------------------------------------

    'unitTypes': ['C', 'N', 'D'],
    'unitTypesAdd':  [None, 'P', 'H'],

    ## ------------ 6. Plot ------------------------------
    'yearStep': 2,                  # x-axis numbers of years per ticklable
    'nbofvariations': 5.0,
    'randomness': 'yes',             # yes or no
    'location': 'office'            # 'office' or 'home'
    }
