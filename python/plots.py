# vim: set tabstop=4
# s_plots.py
#!/usr/bin/env python27
""" Main function to run the code Hug and Maurer created 2011

to do: Check all parameters in the file inputparameter including the path to
the working folder"""

# Copyright (C) 2016 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 04.05.2016

import json
import matplotlib
import matplotlib.pyplot as plt
import pylab
from functions import niceplot, niceplotNoYears
from inputdict import inputdata as inp

## --------------- 3. Plots ---------------------------
## ----------------------------------------------------

## Plot of generated data with the sum of truely working units and
## total amount of units installed for the startup phase.
OUTFILE = '../data/mpm.json'

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'Arial'
matplotlib.rcParams.update({'font.size': 12})

with open(OUTFILE) as json_data:
    MPM = json.load(json_data)
    json_data.close()
pm = MPM[0]
for i in range(0, 3):
    plt.figure(inp['timeMax']/100+2)
    # niceplotNoYears()
    plt.title('Construction of units', y=1.05)
    plt.scatter(MPM[i]['x'], MPM[i]['ynumberOfUnits'], c='r', marker="o",
                edgecolor='none', label='total')
    plt.scatter(MPM[i]['x'], MPM[i]['yworkingUnits'], c='b', marker=".",
                edgecolor='none', label='working')
    plt.scatter(MPM[i]['x'], MPM[i]['yfailedUnits'], c='k', marker=".",
                edgecolor='none', label='failed')
    plt.xlabel('Time [years]')
    plt.ylabel('% of all units')
    plt.legend(loc=5,prop={'size':12}) # set location of legend
    pylab.xlim(xmin=0)
    pylab.ylim(ymin=0)
    plt.show()

## ----------------------------------------------------
## Next plot
## Average System Performance every day

plt.figure(inp['timeMax']/100+3)
# niceplotNoYears()
plt.title('Average treatment system performance every day', y=1.05)
plt.scatter(MPM[0]['x'], MPM[0]['systemPerfVector'], c='r', marker="o",
            edgecolor='none', label='total')
plt.xlabel('Time [years]')
plt.ylabel('Average reduction in %')
plt.legend(loc=5,prop={'size':12})
pylab.xlim(xmin=0)
pylab.ylim(ymin=0.1, ymax=0.9)

## Sensor error type per time step
plt.figure(inp['timeMax']/100+4)
# niceplotNoYears()
plt.title('Sensor measurements', y=1.05)
plt.scatter(MPM[0]['x'], MPM[0]['ySensorfp'], c='limegreen', marker=".",
            edgecolor='none', label='false positive')
plt.scatter(MPM[0]['x'], MPM[0]['ySensortp'], c='g', marker=".",
            edgecolor='none', label='correct positive')
plt.scatter(MPM[0]['x'], MPM[0]['ySensorfn'], c='r', marker=".",
            edgecolor='none', label='false negative')
plt.scatter(MPM[0]['x'], MPM[0]['ySensortn'], c='maroon', marker=".",
            edgecolor='none', label='correct negative')
# plt.scatter(MPM[0]['x'], MPM[0]['ySensorsf'], c='navy', marker=".",
# edgecolor='none', label='sensor failure')
plt.xlabel('Time [years]')
plt.ylabel('# of Sensors')
plt.legend(loc='center', bbox_to_anchor=(0.5, 1.15),
          ncol=2, prop={'size':12})
pylab.xlim(xmin=0)
pylab.ylim(ymin=0)

## Plot of the undetected failures. Stops with every inspection
#plt.figure(2)
#plt.scatter(x, yunitPerformance, c='b', marker="s", label='undetected failure')
#plt.xlabel('Time [days]')
#plt.ylabel('Number of units [# of units]')
#plt.legend()

#plt.figure(inp['timeMax']/100+2)
#niceplot()
#plt.title('Construction of units', y=1.05)
#plt.scatter(MPM[1]['x'], d['yworkingUnits'][0], c='r', marker="o", edgecolor='none', label='failurerate')
#plt.scatter(MPM[1]['x'], d['yworkingUnits'][1], c='b', marker=".", edgecolor='none', label='fr')
#plt.scatter(MPM[1]['x'], d['yworkingUnits'][2], c='k', marker=".", edgecolor='none', label='fr')
#plt.xlabel('Time [years]')
#plt.ylabel('% of all units')
#plt.legend(loc=5,prop={'size':12})                           # set location of legend
#pylab.xlim(xmin=0)
#pylab.ylim(ymin=0)
#plt.show()

''' plot of the different failure shapes of the weibull distribution in
comparison.
main: MPM = variation(1, 4, inp['nbofvariations'], 'shape_fail')'''
plt.figure(inp['timeMax']/100+6)                    # creates figure
# niceplotNoYears()                                   # own function niceplot()
plt.title('failure frequency of sensors', y=1.07)# title
plt.scatter(MPM[0]['x'], MPM[0]['yworkingUnits'], c='r', marker=".",
            edgecolor='none', label='often')    # settings for data 1
plt.scatter(MPM[1]['x'], MPM[1]['yworkingUnits'], c='b', marker=".",
            edgecolor='none', label='less often')    # settings for data 2
plt.scatter(MPM[2]['x'], MPM[2]['yworkingUnits'], c='k', marker=".",
            edgecolor='none', label='rarely')
plt.scatter(MPM[3]['x'], MPM[3]['yworkingUnits'], c='g', marker=".",
            edgecolor='none', label='nearly never')
plt.xlabel('Time [years]')                          # x lable
plt.ylabel('# unit working')                        # y lable
plt.legend(loc=7,prop={'size':12})                  # set location of legend
pylab.xlim(xmin=0)                                  # start of x axis
pylab.ylim(ymin=0, ymax=1100)                       # min and max of yaxis
plt.show()                                          # shows plot

#plt.figure(inp['timeMax']/100+7)
#niceplot()
#plt.title('shape of failure for 1000 units', y=1.05)
#plt.scatter(MPM[0]['x'], MPM[0]['yworkingUnits'], c='r', marker="o", edgecolor='none', label='shape = 2')
#plt.scatter(MPM[1]['x'], MPM[1]['yworkingUnits'], c='b', marker=".", edgecolor='none', label='spape = 4')
##plt.scatter(MPM[2]['x'], MPM[2]['yworkingUnits'], c='maroon', marker=".", edgecolor='none', label='rpd = 210')
#plt.xlabel('Time [years]')
#plt.ylabel('# unit working')
#plt.legend(loc=7,prop={'size':12})                           # set location of legend
#pylab.xlim(xmin=0)
#pylab.ylim(ymin=0, ymax=1100)
#plt.show()
