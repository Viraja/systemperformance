# vim: set tabstop=4
# functions.py
#!/usr/bin/env python3
"""
ToDo:
- sensorAge not used
- include Agents"""
# Copyright (C) 2015 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 16.06.2015

import sys
import unittest

import copy
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import random

from Classes import Unit
from inputdict import inputdata as inp
from inputdict import pythonScriptPath

sys.path.append(pythonScriptPath)

def unitPerformanceModel(timeMax, *, shape=2, scale=412, probTrue=1.0-16/55, probFalse=1.0-1/51,
                  inspfunc=None, alarmThreshold=None,
                  interv=365, maintenancefunc=None):
    '''This is the core function of the model. It checks how many plants are
    performing as expected an how many are not, when a maintenance takes place
    and what the sensors show about the state of a unit. This implementation
    only consideres a binary state for the treatment plant and for the sensors.
    For both they either work or not.'''

    # containers for counting variables
    unitPerformance = []
    sensorDisplay = []
    maintenanceactions = []
    perceivedStateUp = []
    perceivedStateDown = []
    alarmCount = 0

    # initialising the unit and the sensor class.
    UNIT = Unit()

    # making sure that a newly constructed unit is performing as expected.
    UNIT.state = True
    probUpOld = 0.99
    probDownOld = 0.01

    # it is the number of cycles passed.
    for it in range(1, timeMax + 1):
        # regular inspection initialised
        if inspfunc is not None:
            UNIT.state, nbinsp, inspunnec = inspfunc(it, UNIT.state, timeMax,
                                                interv=interv)
        # no inspection takes place
        else:
            nbinsp = 0
            inspunnec = 0

        # Checks if the unit is at its full performance and returns the state
        # or the treatment performance.
        newState = UNIT.checkState(timecount=it, shape=shape, scale=scale)

        # Check what the sensors display and if this
        if maintenancefunc is not None:
            # include sensor display.
            sensordisp = sensorMeasurement(newState, probTrue=probTrue,
                                       probFalse=probFalse)
            if sensordisp == True:
                nbmaint = 0
                maintunnec = 0
                alarmCount = 0
                _perceivedState = 0
            else:
                if alarmThreshold is not None:
                    if alarmCount < alarmThreshold:
                        alarmCount += 1
                        nbmaint = 0
                        maintunnec = 0
                        _perceivedState = 0
                    if alarmCount == alarmThreshold:
                        newState, nbmaint, maintunnec = maintenancefunc(newState)
                        alarmCount = 0
                        _perceivedState = alarmThreshold
                else:
                    newState, nbmaint, maintunnec = maintenancefunc(newState)

            probUpOld, probDownOld = bayes(sensordisp, probUpOld, probDownOld, TP=probTrue, FP=1-probFalse, TN=probFalse, FN=1-probTrue)

        else:
            nbmaint = 0
            maintunnec = 0
            sensordisp = None
            probUpOld = False
            probDownOld = False
        UNIT.state = newState
        unitPerformance.append(newState)
        sensorDisplay.append(sensordisp)
        perceivedStateUp.append(probUpOld)
        perceivedStateDown.append(probDownOld)
        maintenanceactions.append((nbinsp, nbmaint, inspunnec, maintunnec))

    return np.array(((unitPerformance, sensorDisplay, perceivedStateUp, perceivedStateDown), maintenanceactions))

def bayes(sensordisp, probUpOld, probDownOld, TP=1.0, FP=0.0, TN=1.0, FN=0.0):
    if sensordisp == True:
        probUp = (TP * probUpOld) / (TP * probUpOld + FP * probDownOld)
        probDown = (FP * probDownOld) / (FP * probDownOld + TP * probUpOld)
    else:
        sensordisp == False
        probDown = (TN * probDownOld) / (TN * probDownOld + FN * probUpOld)
        probUp = (FN * probUpOld) / (FN* probUpOld + TN * probDownOld)

    if probUp > 0.999:
        probUp = 0.999
    if probDown < 0.01:
        probDown = 0.001

    return (probUp, probDown)

def unitPerformanceModelBayes(timeMax, *, shape=2, scale=412, TP=1.0, FP=0.0,
                              TN=1.0, FN=0.0,
                              inspfunc=None, alarmThreshold=0.98,
                              interv=365, maintenancefunc=None):
    '''This is the core function of the model. It checks how many plants are
    performing as expected an how many are not, when a maintenance takes place
    and what the sensors show about the state of a unit. This implementation
    only consideres a binary state for the treatment plant and for the sensors.
    For both they either work or not.'''

    # containers for counting variables
    unitPerformance = []
    sensorDisplay = []
    maintenanceactions = []
    perceivedUp = []
    perceivedDown = []

    # initialising the unit and the sensor class.
    UNIT = Unit()

    # making sure that a newly constructed unit is performing as expected.
    UNIT.state = True
    probUpOld = 0.99
    probDownOld = 0.01

    # it is the number of cycles passed.
    for it in range(1, timeMax + 1):
        # regular inspection initialised
        oldState = copy.copy(UNIT.state)
        if inspfunc is not None:
            UNIT.state, nbinsp, inspunnec = inspfunc(it, UNIT.state, timeMax,
                                                interv=interv)
        # no inspection takes place
        else:
            nbinsp = 0
            inspunnec = 0

        # Checks if the unit is at its full performance and returns the state
        # or the treatment performance.
        newState = UNIT.checkState(timecount=it, shape=shape, scale=scale)

        # Check what the sensors display and if this
        if maintenancefunc is not None:
            # if newState == False:
            #     import pdb; pdb.set_trace()
            # include sensor display.
            sensordisp = sensorMeasurement(newState, probTrue=TP, probFalse=TN)
            if sensordisp == True:
                probUp = (TP * probUpOld) / (TP * probUpOld + FP * probDownOld)
                probDown = (FP * probDownOld) / (FP * probDownOld + TP * probUpOld)
            else:
                probDown = (TN * probDownOld) / (TN * probDownOld + FN * probUpOld)
                probUp = (FN * probUpOld) / (FN* probUpOld + TN * probDownOld)

            if probDown > alarmThreshold:
                newState, nbmaint, maintunnec = maintenancefunc(newState)
                probUp = 0.99
                probDown = 0.01
            else:
                nbmaint = 0
                maintunnec = 0

            # testsum = probDown + probUp
            # if testsum != 1:
            #     print(testsum)

        else:
            nbmaint = 0
            maintunnec = 0
            sensordisp = None
            probUp = 0.99
            probDown = 0.01

        if probUp > 0.99:
            probUp = 0.99
        if probDown < 0.01:
            probDown = 0.01

        if probDown > 0.999:
            probDown = 0.999
        if probUp < 0.001:
            probUp = 0.001

        UNIT.state = newState
        unitPerformance.append(newState)
        sensorDisplay.append(sensordisp)
        perceivedUp.append(probUp)
        perceivedDown.append(probDown)
        maintenanceactions.append((nbinsp, nbmaint, inspunnec, maintunnec))

        probUpOld = copy.copy(probUp)
        probDownOld = copy.copy(probDown)

    return np.array(((unitPerformance, sensorDisplay, perceivedUp, perceivedDown), maintenanceactions))


def averageUnitPerformance(timemax, **kwargs):

    unitPerformance, maintenanceNumber = unitPerformanceModel(timemax, **kwargs)
    systemPerformance = np.count_nonzero(unitPerformance[0]) / timemax
    display = np.count_nonzero(unitPerformance[1]) / timemax
    perceivedPerformance = sum(unitPerformance[2]) / timemax
    _insp = np.count_nonzero([maintenanceNumber[i][0] for i in range(
        0, timemax)])
    _maint = np.count_nonzero([maintenanceNumber[j][1] for j in range(
        0, timemax)])

    maintenancePerYear = (_insp + _maint) / (timemax / 365)

    return (systemPerformance, perceivedPerformance, display, maintenancePerYear)

def averageUnitPerformanceBayes(timemax, **kwargs):

    unitPerformance, maintenanceNumber = unitPerformanceModelBayes(timemax, **kwargs)
    systemPerformance = np.count_nonzero(unitPerformance[0]) / timemax
    display = np.count_nonzero(unitPerformance[1]) / timemax
    up = sum(unitPerformance[2]) / timemax
    down = sum(unitPerformance[3]) / timemax
    _insp = np.count_nonzero([maintenanceNumber[i][0] for i in range(
        0, timemax)])
    _maint = np.count_nonzero([maintenanceNumber[j][1] for j in range(
        0, timemax)])

    maintenancePerYear = (_insp + _maint) / (timemax / 365)

    return (systemPerformance, display, up, down, maintenancePerYear)

def averageUnitPerformanceBayes2(timemax, **kwargs):

    unitPerformance, maintenanceNumber = unitPerformanceModel(timemax, **kwargs)
    systemPerformance = np.count_nonzero(unitPerformance[0]) / timemax
    display = np.count_nonzero(unitPerformance[1]) / timemax
    up = sum(unitPerformance[2]) / timemax
    down = sum(unitPerformance[3]) / timemax
    _insp = np.count_nonzero([maintenanceNumber[i][0] for i in range(
        0, timemax)])
    _maint = np.count_nonzero([maintenanceNumber[j][1] for j in range(
        0, timemax)])

    maintenancePerYear = (_insp + _maint) / (timemax / 365)

    return (systemPerformance, display, up, down, maintenancePerYear)


def maintenancefunc(unitState):
    if unitState == False:
        workunnecessary = 0
    if unitState == True:
        workunnecessary = 1

    return (True, 1, workunnecessary)

def sensorMeasurement(unitState, *, probTrue=0.75, probFalse=0.75):
    number = random.random()

    if unitState == True:              # if the sensor is still working
        if number < (probTrue):
            return True
        else:
            return False

    else:
        if number < (probFalse):
            return False
        else:
            return True

def shedulInspect(timestep, unitstate, timemax, *, interv=365):
    ''' Regular inspection taking place as often as interv suggests.'''
    if timestep % interv == 0:
        if unitstate == True:
            return (True, 1, 1)
        else:
            return (True, 1, 0)
    else:
        work = 0
        workunnecessary = 0
        if unitstate == True:
            return (True, 0, 0)
        else:
            return (False, 0, 0)

def niceplot(*, time_max=10, year_step=2):
    ''' The function niceplot changes the standard setting of the python plot
    it can be called in any plot to change them in the same way.'''

    ax = plt.subplot(111)                           # subplot framework

    ax.spines['right'].set_visible(False)           # frame line right "off"
    ax.spines['top'].set_visible(False)             # frame line bottom "off"

    ax.xaxis.set_tick_params(width=1.5)            # Size of ticks e.g. 1.5
    ax.yaxis.set_tick_params(width=1.5)            # Size of ticks e.g. 1.5

    ax.tick_params(axis='y', direction='out')       # Ticks outwards with 'out'
    ax.tick_params(axis='x', direction='out')       # Ticks outwards with 'out'

    ax.xaxis.set_ticks_position('bottom')           # Only tiks on bottom
    ax.yaxis.set_ticks_position('left')             # Only ticks on left
    # x-axis in years instead of hours
    ax.xaxis.set_major_locator(plt.FixedLocator(range(0, time_max + 1,
                                                      year_step * 365)))
    # ticklabels
    ax.xaxis.set_ticklabels(range(0, time_max / 365 + 1,
                                  year_step))
    plt.gcf().subplots_adjust(bottom=0.15)          # for larger font size...
    plt.gcf().subplots_adjust(left=0.15)            # ...not to be cut
    # fig.tight_layout(pad=0.1)

def niceplotNoYears(*, time_max=10):
    ''' The function niceplot changes the standard setting of the python plot
    it can be called in any plot to change them in the same way.'''
    ax = plt.subplot(111)                           # subplot framework

    ax.spines['right'].set_visible(False)           # frame line right "off"
    ax.spines['top'].set_visible(False)             # frame line bottom "off"

    ax.xaxis.set_tick_params(width=1.5)             # Size of ticks e.g. 1.5
    ax.yaxis.set_tick_params(width=1.5)             # Size of ticks e.g. 1.5

    ax.tick_params(axis='y', direction='out')       # Ticks outwards with 'out'
    ax.tick_params(axis='x', direction='out')       # Ticks outwards with 'out'

    ax.xaxis.set_ticks_position('bottom')           # Only tiks on bottom
    ax.yaxis.set_ticks_position('left')             # Only ticks on left

    # x-axis in years instead of hours
    ax.xaxis.set_major_locator(plt.FixedLocator(range(0, time_max + 1,
                                                      365)))
                                                    # where to put ticks
    ax.xaxis.set_ticklabels(range(0, int(time_max / (365) + 1), 1))
                                                    # determined ticklables
    plt.gcf().subplots_adjust(bottom=0.15)          # for larger font size...
    plt.gcf().subplots_adjust(left=0.15)            # ...not to be cut off
    # fig.tight_layout(pad=0.1)

if __name__ == '__main__':
#     unittest.main()
    SYSTEMPERF = []
    WORK = []
    NBUNITS = 5
    for iu in range(0, NBUNITS):
        UNIT = Unit()
        SENSOR = Sensor()
        UNITPERFORMANCE, COSTS = unitPerformanceModel(
            10*365, inspfunc=shedulInspect, maintenancefunc=maintenancefunc,
            alarmThreshold=7)
        SYSTEMPERF.append(np.array(UNITPERFORMANCE).copy())
        WORK.append(np.array(COSTS).copy())

    sysperf = [np.count_nonzero(np.array(SYSTEMPERF)[i][0]) for i in range(
        0, len(SYSTEMPERF))]
    maint = [np.count_nonzero(np.array(SYSTEMPERF)[i][1]) for i in range(
        0, len(SYSTEMPERF))]


    c1 = np.count_nonzero([(np.array(WORK)[0][i][0]) for i in range(
        0, len(WORK[0]))])
    c2 = np.count_nonzero([(np.array(WORK)[1][i][0]) for i in range(
        0, len(WORK[0]))])
    c3 = np.count_nonzero([(np.array(WORK)[2][i][0]) for i in range(
        0, len(WORK[0]))])
    c4 = np.count_nonzero([(np.array(WORK)[3][i][0]) for i in range(
        0, len(WORK[0]))])
    c5 = np.count_nonzero([(np.array(WORK)[4][i][0]) for i in range(
        0, len(WORK[0]))])

    m1 = np.count_nonzero(np.array([(np.array(WORK)[0][i][1]) for i in range(
        0, len(WORK[0]))]))
    m2 = np.count_nonzero([(np.array(WORK)[1][i][1]) for i in range(
        0, len(WORK[0]))])
    m3 = np.count_nonzero([(np.array(WORK)[2][i][1]) for i in range(
        0, len(WORK[0]))])
    m4 = np.count_nonzero([(np.array(WORK)[3][i][1]) for i in range(
        0, len(WORK[0]))])
    m5 = np.count_nonzero([(np.array(WORK)[4][i][1]) for i in range(
        0, len(WORK[0]))])

    ui1 = np.count_nonzero([(np.array(WORK)[0][i][2]) for i in range(
        0, len(WORK[0]))])
    ui2 = np.count_nonzero([(np.array(WORK)[1][i][2]) for i in range(
        0, len(WORK[0]))])
    ui3 = np.count_nonzero([(np.array(WORK)[2][i][2]) for i in range(
        0, len(WORK[0]))])
    ui4 = np.count_nonzero([(np.array(WORK)[3][i][2]) for i in range(
        0, len(WORK[0]))])
    ui5 = np.count_nonzero([(np.array(WORK)[4][i][2]) for i in range(
        0, len(WORK[0]))])

    um1 = np.count_nonzero([(np.array(WORK)[0][i][3]) for i in range(
        0, len(WORK[0]))])
    um2 = np.count_nonzero([(np.array(WORK)[1][i][3]) for i in range(
        0, len(WORK[0]))])
    um3 = np.count_nonzero([(np.array(WORK)[2][i][3]) for i in range(
        0, len(WORK[0]))])
    um4 = np.count_nonzero([(np.array(WORK)[3][i][3]) for i in range(
        0, len(WORK[0]))])
    um5 = np.count_nonzero([(np.array(WORK)[4][i][3]) for i in range(
        0, len(WORK[0]))])



    sysperfpercent = np.array(sysperf)/(len(SYSTEMPERF[0][0]))
    maintenance = np.array(maint)/(len(SYSTEMPERF[0][1]))

    plt.plot()
