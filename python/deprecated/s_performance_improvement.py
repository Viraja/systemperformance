# vim: set tabstop=4
# s_performance_improvement.py
#!/usr/bin/env python3
""" Create a plot which draws robustness versus system performance for 
different monitoring schemes."""

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 07.01.2020

import json
import matplotlib
import matplotlib.pyplot as plt
import pylab
from functions import niceplot, niceplotNoYears
from inputdict import inputdata as inp

## --------------- 3. Plots ---------------------------
## ----------------------------------------------------

## Plot of generated data with the sum of truely working units and
## total amount of units installed for the startup phase.
OUTFILE = '../data/mpm.json'

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'Arial'
matplotlib.rcParams.update({'font.size': 12})

with open(OUTFILE) as json_data:
    MPM = json.load(json_data)
    json_data.close()

plt.figure(inp['timeMax'] / 100+2)
niceplotNoYears()
plt.title('Construction of units', y=1.05)
plt.scatter(MPM[1]['x'], MPM[1]['ynumberOfUnits'], c='r', marker="o",
            edgecolor='none', label='total')
plt.scatter(MPM[1]['x'], MPM[1]['yworkingUnits'], c='b', marker=".",
            edgecolor='none', label='working')
plt.scatter(MPM[1]['x'], MPM[1]['yfailedUnits'], c='k', marker=".",
            edgecolor='none', label='failed')
plt.xlabel('Time [years]')
plt.ylabel('% of all units')
plt.legend(loc=5,prop={'size':12}) # set location of legend
pylab.xlim(xmin=0)
pylab.ylim(ymin=0)
plt.show()
