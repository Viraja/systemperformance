# vim: set tabstop=4
# s_sensor_scenarios.py
#!/usr/bin/env python3
""" Function to plto system performance against robustnes for """

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 14.01.2020

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
from collections import namedtuple
import json
import matplotlib.pyplot as plt
import pylab

from inputdict import inputdata as inp
from functions import pareto, variation


Scenarios = namedtuple("Scenarios", ["filename", "inp1", "minimum", "maximum", "steps",
                                    "inp2", "val2", "inp3", "val3", "inp4",
                                    "val4", "color", "size", "label"])

DATAPATH = '../data/'
SCENARIO = (
    Scenarios(filename=DATAPATH+"sensor_a025", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='failureRateSensor', val2=0.0, inp3=None,
              val3=None, inp4=None, val4=None, color="aqua", size=15, label='100%'),
    Scenarios(filename=DATAPATH+"sensor_a025", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='failureRateSensor', val2=0.25, inp3=None,
              val3=None, inp4=None, val4=None, color="blue", size=10, label='75%'),
    Scenarios(filename=DATAPATH+"sensor_a050", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='failureRateSensor', val2=0.5, inp3=None,
              val3=None, inp4=None, val4=None, color="deepskyblue", size=5, label='50%'),
    Scenarios(filename=DATAPATH+"sensor_a075", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='failureRateSensor', val2=0.75, inp3=None,
              val3=None, inp4=None, val4=None, color="black", size=1, label='25%'),
    Scenarios(filename=DATAPATH+"sensor_a100", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='failureRateSensor', val2=1.0, inp3=None,
              val3=None, inp4=None, val4=None, color="limegreen", size=5, label='0%'),
    Scenarios(filename=DATAPATH+"no_sensor", inp1='scale_fail', minimum=1,
              maximum=3650, steps=20, inp2='sensorIs', val2=0, inp3=None,
              val3=None, inp4=None, val4=None, color="darkgreen", size=1, label='no sensor')
    )

def writefiles(scenario):
    '''
    '''
    names = []
    for x in scenario:
        fname = x.filename.split(DATAPATH, 1)[1]
        names.append(fname)
    MPM = dict().fromkeys(names)
    for x in SCENARIO:
        fname = x.filename.split(DATAPATH, 1)[1]
        MPM[fname] = variation(inp1=x.inp1, minimum=x.minimum, maximum=x.maximum, steps=x.steps,
                               inp2=x.inp2, val2=x.val2)

        OUTFILE = DATAPATH + 'mpm_test_' + fname + '.json'

        with open(OUTFILE, 'w', encoding='utf-8') as f:
            json.dump(MPM[fname], f, ensure_ascii=False, indent=4, separators=(',', ': '))
            f.close()

if __name__ == '__main__':
    import argparse

    ## Parse command line arguments to show write or read
    parser = argparse.ArgumentParser(description='Choose reading or writing.')
    parser.add_argument(
        '-w', '--write',
        help='If present, simulates data and writes them to a file.',
        action='store_true')
    parser.add_argument(
        '-f1', '--figure1',
        help='Plots Figure 1.',
        action='store_true')
    parser.add_argument(
        '-f2', '--figure2',
        help='Plots Figure 2.',
        action='store_true')
    args = parser.parse_args()

    if args.write:
        writefiles(SCENARIO)
        print('write')
    if args.figure1:
        print('plotting figure 1')

    i = 1

    plt.figure()
    plt.title('failure frequency of sensors', y=1.07)# title
    for x in SCENARIO:
        fname = x.filename.split(DATAPATH, 1)[1]
        with open(DATAPATH + 'mpm_test_' + fname + '.json') as json_data:
            MPM = json.load(json_data)
            json_data.close()
        # niceplotNoYears()                                   # own function niceplot()
        if i == 1:
            plt.plot(MPM[0]['x'], MPM[0]['yworkingUnits'], color='r', marker=".",
                        markeredgecolor='none', label='nearly never')    # settings for data 1
            plt.plot(MPM[1]['x'], MPM[1]['yworkingUnits'], color='b', marker=".",
                        markeredgecolor='none', label='rarely')    # settings for data 2
            plt.plot(MPM[2]['x'], MPM[2]['yworkingUnits'], color='k', marker=".",
                        markeredgecolor='none', label='less often')
            plt.plot(MPM[3]['x'], MPM[3]['yworkingUnits'], color='g', marker=".",
                        markeredgecolor='none', label='often')
            i += 1
        else:
            plt.plot(MPM[0]['x'], MPM[0]['yworkingUnits'], color='r', marker=".",
                        markeredgecolor='none')    # settings for data 1
            plt.plot(MPM[1]['x'], MPM[1]['yworkingUnits'], color='b', marker=".",
                        markeredgecolor='none')    # settings for data 2
            plt.plot(MPM[2]['x'], MPM[2]['yworkingUnits'], color='k', marker=".",
                        markeredgecolor='none')
            plt.plot(MPM[3]['x'], MPM[3]['yworkingUnits'], color='g', marker=".",
                        markeredgecolor='none')

        plt.xlabel('Time [years]')                          # x lable
        plt.ylabel('# unit working')                        # y lable
        # plt.legend(loc=7,prop={'size':12})                  # set location of legend
        pylab.xlim(xmin=0)                                  # start of x axis
        pylab.ylim(ymin=0, ymax=1000)                       # min and max of yaxis
    plt.legend(loc=7, prop={'size':12})
    plt.show()

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
    for x in SCENARIO:
        fname = x.filename.split(DATAPATH, 1)[1]
        with open(DATAPATH + 'mpm_test_' + fname + '.json') as json_data:
            MPM = json.load(json_data)
            json_data.close()
        for i in range(0, len(MPM)):
        # niceplotNoYears()                                   # own function niceplot()
            sumperf = 0
            for j in range(0, len(MPM[i]['yworkingUnits'])):
                sumperf += MPM[i]['yworkingUnits'][j]
            perfsum = sumperf/(MPM[i]['IData']['timeMax']*MPM[i]['IData']['nbOfUnits'])
            if i == 1:
                 ax.scatter(MPM[i]['IData']['scale_fail'], perfsum, c=x.color,
                             marker=".", edgecolor='none', label=x.label,
                             s=x.size*100)    # settings for data 1
            else:
                ax.scatter(MPM[i]['IData']['scale_fail'], perfsum, c=x.color,
                            marker=".", edgecolor='none',
                            s=x.size*100)    # settings for data 1
        ax.set_xlabel('robustness of OST')
        ax.set_ylabel('treatment performance of system')


        # plt.xlabel('Robustness plant')                          # x lable
        # plt.ylabel('% of unit functioning')                        # y lable
        # plt.legend(loc=7,prop={'size':12})                  # set location of legend
        # pylab.xlim(xmin=0)                                  # start of x axis
        # pylab.ylim(ymin=0, ymax=1)                       # min and max of yaxis
        fig.tight_layout(pad=0.01)
    plt.legend(loc=4, prop={'size':12})
    plt.show()

    # plt.figure()
    # plt.title('random sensors', y=1.07)# title
    # for x in SCENARIO:
    #     fname = x.filename.split(DATAPATH, 1)[1]
    #     with open(DATAPATH + 'mpm_' + fname + '.json') as json_data:
    #         MPM = json.load(json_data)
    #         json_data.close()
    #     for i in range(0, len(MPM)):
    #     # niceplotNoYears()                                   # own function niceplot()
    #         sumperf = 0
    #         for j in range(0, len(MPM[i]['yworkingUnits'])):
    #             sumperf += MPM[i]['yworkingUnits'][j]
    #         perfsum = sumperf/(MPM[i]['IData']['timeMax']*MPM[i]['IData']['nbOfUnits'])
    #
    #         plt.scatter(MPM[i]['IData']['scale_fail'], perfsum, c=x.color, marker=".",
    #                 edgecolor='none', label='often', s=x.size*100)    # settings for data 1
    #
    #     plt.xlabel('Robustness plant')                          # x lable
    #     plt.ylabel('# unit working')                        # y lable
    #     # plt.legend(loc=7,prop={'size':12})                  # set location of legend
    #     pylab.xlim(xmin=0)                                  # start of x axis
    #     pylab.ylim(ymin=0, ymax=1)                       # min and max of yaxis
    # # plt.legend(loc=7,prop={'size':12})
    # plt.show()

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
    for x in SCENARIO:
        fname = x.filename.split(DATAPATH, 1)[1]
        with open(DATAPATH + 'mpm_test_' + fname + '.json') as json_data:
            MPM = json.load(json_data)
            json_data.close()
        for i in range(0, len(MPM)):
        # niceplotNoYears()                                   # own function niceplot()
            sumperf = 0
            suminsp = 0
            for j in range(0, len(MPM[i]['yworkingUnits'])):
                sumperf += MPM[i]['yworkingUnits'][j]
                suminsp += MPM[i]['ynbOfInspectionTot'][j]
            perfsum = sumperf / (MPM[i]['IData']['timeMax'] * MPM[i]['IData']['nbOfUnits'])
            inspsum = suminsp / (MPM[i]['IData']['timeMax'] / 365) / (MPM[i]['IData']['nbOfUnits'])#/(MPM[i]['IData']['timeMax']*MPM[i]['IData']['nbOfUnits'])
            alph = float(MPM[i]['IData']['scale_fail']) / 3650
            if alph < 0.7:
                alph += 0.3
            else:
                alph = 1
            if i == len(MPM)-1:
                ax.scatter(inspsum, perfsum, c=x.color, marker=".", alpha=alph,
                           edgecolor='none', label=x.label, s=x.size*100)    # settings for data 1
            else:
                ax.scatter(inspsum, perfsum, c=x.color, marker=".", alpha=alph,
                           edgecolor='none', s=x.size*100)
            # plt.scatter(MPM[i]['ynbOfRepair'], MPM[i]['yworkingUnits'], c=x.color, marker=".",
            #         edgecolor='none', label='often', s=x.size*100)    # settings for data 1

        ax.set_xlabel('amount of maintenance requests')                          # x lable
        ax.set_ylabel('treatment performance of system')                        # y lable
        # plt.legend(loc=7,prop={'size':12})                  # set location of legend
        # pylab.xlim(xmin=0)                                  # start of x axis
        # pylab.ylim(ymin=0, ymax=1)                       # min and max of yaxis
        ax.set_xscale('log')
        fig.tight_layout(pad=0.01)
    plt.legend(loc=8, prop={'size':12})
    plt.show()
