# vim: set tabstop=4
# s_sensor_scenarios.py
#!/usr/bin/env python3
""" Function to plto system performance against robustnes for """

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 28.03.2020

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
from collections import namedtuple
from functools import partial
import itertools
# from joblib import (
#     Parallel,
#     delayed)
import json
import math
import matplotlib.pyplot as plt
import numpy as np

from scipy.special import gamma

# from Classes import Unit
from functions import unitPerformanceModel, maintenancefunc, shedulInspect

Scenarios = namedtuple("Scenarios", ["filename", "shape", "scale", "probTrue",
                                     "probFalse", "inspfunc", "alarmThreshold",
                                     "interv", "maintenancefunc", "color",
                                     "size", "label"])
DATAPATH = '../data/'
SHAPE = 2
TIMEMAX = 10*365

SCENARIO = [
    # Scenarios(filename=DATAPATH+"alarmThreshold1", shape=SHAPE, scale=412,
    #           probTrue=16/55, probFalse=1/51, inspfunc=None, alarmThreshold=1,
    #           interv=3651, maintenancefunc=maintenancefunc, color="blue",
    #           size=10, label='a')#,
    # Scenarios(filename=DATAPATH+"alarmThreshold4", shape=SHAPE, scale=412,
    #           probTrue=16/55, probFalse=1/51, inspfunc=None, alarmThreshold=4,
    #           interv=365, maintenancefunc=maintenancefunc, color="aqua",
    #           size=10, label='b'),
    # Scenarios(filename=DATAPATH+"alarmThreshold14", shape=SHAPE, scale=412,
    #           probTrue=16/55, probFalse=1/51, inspfunc=None, alarmThreshold=14,
    #           interv=182, maintenancefunc=maintenancefunc, color="blue",
    #           size=10, label='c'),
    Scenarios(filename=DATAPATH+"alarmThresholdAdaptive", shape=SHAPE, scale=412,
              probTrue=16/55, probFalse=1/51, inspfunc=None,
              alarmThreshold="adaptive",
              interv=1, maintenancefunc=maintenancefunc, color="blue",
              size=10, label='d')
    ]

def writefiles(scenario, variation1=None, variation2=None):
    '''
    Function to write for a given namedtuple with scenarios the results of a
    grid search.

    Arguments
    ---------
    scenario : namedtuple
       Contains all arguments and keyword arguments for the unitPerformanceModel
       function, as well as the lables and plotting properties.

    Keyword arguments
    -----------------
    variation1 : array
        all variations of the first variable for the grid search
    variation2 : array
        all variantions of the second variable for the grid search
    '''

    _fnames = []

    for x in scenario:
        fname = x.filename.split(DATAPATH, 1)[1]
        _fnames.append(fname)
    UNITPERF = dict().fromkeys(_fnames)
    MAINT = dict().fromkeys(_fnames)
    CONTROL = dict().fromkeys(_fnames)

    if (variation1 is not None) and (variation2 is not None):
        parameters = np.array(list(itertools.product(
            variation1, variation2)), ndmin=2)
    else:
        parameters = None

    for x in scenario:
        perf = []
        maint = []
        cperf = []
        cmaint = []
        fname = x.filename.split(DATAPATH, 1)[1]

        if parameters is not None:

            for param in parameters:
                if str(x.alarmThreshold) == "adaptive":
                    testfunc = partial(
                        unitPerformanceModel, TIMEMAX, shape=x.shape,
                        scale=param[0], probTrue=param[1], probFalse=param[1],
                        inspfunc=x.inspfunc,
                        alarmThreshold=1 + math.ceil(np.log(1-0.98) / \
                            np.log(1-param[1])),
                        interv=x.interv, maintenancefunc=x.maintenancefunc)
                else:
                    testfunc = partial(
                        unitPerformanceModel, TIMEMAX, shape=x.shape,
                        scale=param[0], probTrue=param[1], probFalse=param[1],
                        inspfunc=x.inspfunc, alarmThreshold=x.alarmThreshold,
                        interv=x.interv, maintenancefunc=x.maintenancefunc)
                _unitperf, _maint = testfunc()
                perf.append(_unitperf)
                maint.append(_maint)

        UNITPERF[fname] = perf
        MAINT[fname] = maint

        if parameters is None:
            UNITPERF[fname], MAINT[fname] = unitPerformanceModel(
                TIMEMAX, shape=x.shape, scale=x.scale, probTrue=x.probTrue,
                probFalse=x.probFalse, inspfunc=x.inspfunc,
                alarmThreshold=x.alarmThreshold,
                interv=x.interv, maintenancefunc=x.maintenancefunc)


        for var in variation1:
            _cp = []
            _cm = []
            for counti in range(0, 1000):
                _cperf, _cmaint = unitPerformanceModel(
                    TIMEMAX, shape=x.shape, scale=var,
                    inspfunc=shedulInspect,
                    # inspfunc=None,
                    maintenancefunc=None,
                    interv=x.interv)
                _cp.append(np.count_nonzero(_cperf[0]) / TIMEMAX)
                _cm.append((np.count_nonzero(_maint[0]) + np.count_nonzero(
                    _maint[1])) / (TIMEMAX / 365))
            cperf.append(sum(_cp) / len(_cp))
            cmaint.append(sum(_cm) / len(_cm))
            print(var, sum(_cp) / len(_cp))
            print(var, sum(_cm) / len(_cm))

        CONTROL[fname] = cperf

        OUTFILE1 = DATAPATH + 'performance_' + fname + '.json'
        OUTFILE2 = DATAPATH + 'maintenance_' + fname + '.json'
        control = DATAPATH + 'control_' + fname + '.json'

        with open(OUTFILE1, 'w', encoding='utf-8') as f:
            json.dump(UNITPERF[fname], f, ensure_ascii=False, indent=4,
                      separators=(',', ': '))
            f.close()
        with open(OUTFILE2, 'w', encoding='utf-8') as f:
            json.dump(MAINT[fname], f, ensure_ascii=False, indent=4,
                      separators=(',', ': '))
            f.close()
        with open(control, 'w', encoding='utf-8') as f:
            json.dump(CONTROL[fname], f, ensure_ascii=False, indent=4,
                      separators=(',', ': '))
            f.close()
        print('Saving results to %s ...' % OUTFILE1)
#
# def partialwrapper(parameters):
#     return partial(
#         unitPerformanceModel, TIMEMAX, shape=x.shape, scale=param[0],
#         probTrue=param[1], probFalse=param[1], inspfunc=x.inspfunc,
#         alarmThreshold=x.alarmThreshold, interv=x.interv,
#         maintenancefunc=x.maintenancefunc)

if __name__ == '__main__':
    import argparse

    ## Parse command line arguments to show write or read
    parser = argparse.ArgumentParser(description='Choose reading or writing.')
    parser.add_argument(
        '-w', '--write',
        help='If present, simulates data and writes them to a file.',
        action='store_true')
    args = parser.parse_args()

    numberVariations = 20 # number of time the input variables are varied
    scale = np.linspace(100, 2500, numberVariations)
    probTF = np.linspace(0.5, 1.0, numberVariations)
    parameters = np.array(list(itertools.product(scale, probTF)), ndmin=2)

    if args.write:
        writefiles(SCENARIO, variation1=scale, variation2=probTF)
        print('write')

    # stores all names for the different scenarios
    _names = []
    for x in SCENARIO:
        fname = x.filename.split(DATAPATH, 1)[1]
        _names.append(fname)

    UNITPERF = dict().fromkeys(_names)
    MAINT = dict().fromkeys(_names)
    PERF = dict().fromkeys(_names)
    MAIN = dict().fromkeys(_names)
    INSP = dict().fromkeys(_names)
    MAINUN = dict().fromkeys(_names)
    INSPUN = dict().fromkeys(_names)
    CONTROL = dict().fromkeys(_names)
    PERFCONT = dict().fromkeys(_names)


    dim = len(parameters)
    betas = np.array([0.5, 1, 2])

    if dim >= 2:

        CMAP = 'inferno'
        _X0, Y0 = np.meshgrid(scale, probTF)
        X0 = _X0 * gamma(1 + 1 / SHAPE)
        Xcontrol = scale * gamma(1 + 1 / SHAPE)

        for x in SCENARIO:
            fname = x.filename.split(DATAPATH, 1)[1]
            with open(DATAPATH + 'performance_' + fname + '.json') as json_data:
                UNITPERF[fname] = json.load(json_data)
            with open(DATAPATH + 'maintenance_' + fname + '.json') as json_data:
                MAINT[fname] = json.load(json_data)
            with open(DATAPATH + 'control_' + fname + '.json') as json_data:
                CONTROL[fname] = json.load(json_data)

            PERF[fname] = [np.count_nonzero(UNITPERF[fname][i][0]) / \
                TIMEMAX for i in range(0, len(parameters[:, 0]))]

            fig = plt.figure(figsize=(3.54, 3.54), constrained_layout=True)
            _axes = fig.subplots(2, 2, sharex='col', gridspec_kw={
                'height_ratios':[1, 6], 'width_ratios':[10, 1]})
            ax_nv, axes = _axes[:, 0]
            _axes[0, 1].set_axis_off()
            _axes[0, 1].text(0.5, 0.5, x.label+")", ha='center', va='center',
                             size=20)

            M = np.array(PERF[fname])

            ax_nv.scatter(Xcontrol, CONTROL[fname], c=CONTROL[fname], cmap=CMAP,
                          vmin=0, vmax=1)
            ax_nv.set_ylabel('control')
            ax_nv.set_ylim(0.0, 1.0)
            ax_nv.set_yticks([0.0, 1.0])

            ax_nv.xaxis.tick_top()
            ax_nv.tick_params(labeltop=False)
            pcm = axes.pcolormesh(X0, Y0, np.transpose(M.reshape(Y0.shape)),
                                  cmap=CMAP, vmin=0, vmax=1)
            cbar = plt.colorbar(pcm, cax=_axes[1, 1])
            cbar.set_label('fraction OST unit available', rotation=90)
            axes.set_xlabel('mean survival time (days)')
            axes.set_ylabel('sensor accuracy')
            axes.xaxis.tick_bottom()
            fig.tight_layout(pad=0.01)
            plt.show()

            fig = plt.figure(figsize=(3.54, 3.54))
            axes = fig.subplots(1, 1)

            MAIN[fname] = [[MAINT[fname][j][i][1] for i in range(
                0, TIMEMAX)] for j in range(0, len(parameters[:, 0]))]
            INSP[fname] = [[MAINT[fname][j][i][0] for i in range(
                0, TIMEMAX)] for j in range(0, len(parameters[:, 0]))]
            MAINUN[fname] = [[MAINT[fname][j][i][3] for i in range(
                0, TIMEMAX)] for j in range(0, len(parameters[:, 0]))]
            INSPUN[fname] = [[MAINT[fname][j][i][2] for i in range(
                0, TIMEMAX)] for j in range(0, len(parameters[:, 0]))]

            N = np.array([np.count_nonzero(MAIN[fname][i]) + \
                np.count_nonzero(INSP[fname][i]) for i in range(
                    0, len(MAIN[fname]))])/10

            pcm = axes.pcolormesh(X0, Y0, np.transpose(
                N.reshape(X0.shape)), cmap=CMAP, vmin=0, vmax=8)
            cbar = plt.colorbar(pcm, ax=axes)
            cbar.set_label('maintenance done per year')
            axes.set_xlabel('mean survival time [days]')
            axes.set_ylabel('sensor accuracy')
            axes.set_title(x.label + ")", loc='left', size=20)
            fig.tight_layout(pad=0.01)
            plt.show()
