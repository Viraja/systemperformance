#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 15:54:28 2015

@author: Mariane Schneider (mariane.schneider@eawag.ch)

content: This class defines the Wastewater treatment units. Giving it some 
state, a failure time and attaching a sensor to it. 

to do: 

version: 0.22 for Pyton 2.7
"""

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
import sys
import random
import numpy as np
import pdb
import math

from inputdict import pythonScriptPath
from inputdict import inputdata as inp
#from collections import defaultdict

sys.path.append(pythonScriptPath)

## ------------- 1. Unit Class definition ------------
## ---------------------------------------------------

class Unit(object):
    '''Makes a class named Unit that is an object and represents all 
    Wastewater treatment units that are installed.'''
    
    def __init__(self, state = True):
        '''Initial settings for wastewater treatment units
        This includes the step by step construction of the unit in 
        the initial phase'''
        
        self.timeConstructed = self.construction(inp['constructionVariation'])
        self.unitAge = 0
        self.state = state
        self.performance = self.perfDistribution()
        self.sensor = Sensor()              # appends to every Unit a Sensor
        self.nbUnitsWaitingRepair = 0
        self.nbUnitsWaitingInsp = 0
        self.timeInspection = 0    
        self.failuretime = 0
        
#        self.unitType = self.unittype()
#        self.unitTypeAdd = self.unittypeadd()


    def construction(self, timeSlot = inp['constructionVariation']):
        '''Determines when the treatment unit was constructed. If the value
        in the inputparameter file is set to 0 than all unit where constructed
        at t = 0'''

        if timeSlot < 365:                  # in the first year no Variation
            result1 = 1                     # constructs all units at time 1
        else:
            result1 = int(round(random.random()*timeSlot))
                                            # random distribution within Time
        return result1                      # returns the constructiontime
        
        
    def update(self, timecount, shape, scale):
        '''Checks depending on the failure rate whether a sensor is still
        working properly. And define what kind of failure it is. '''
        
        if inp['randomness'] == 'no':
            if self.failuretime > scale:
                self.state = False
                self.failuretime == 0
        else:        
            val = (shape/scale*((timecount/scale)**(shape-1.0)))
                                            # weibull distributed hazard rate
            randomNumber = np.random.random()   
                                            # random number between 0 and 1
            if randomNumber < val:          # check if failed
                self.state = False          # Unit failed 
            
            
    def repair(self, daycount, techWorkingHoursLeft):
        '''repaire function with repaires if personnel is still available and
        only on weekdays. Can be turned of in inputdict.'''
        
        weekday = daycount % 7              # daycount (it) devided by 7
        if ((weekday == 5) or (weekday == 6)) and (inp['weekend'] == 'on'):
            '''For Saturday and Sunday no work'''
            pass                            # nothing happens
        ## if no weekend and personnel availabe the unit is repaired
        elif techWorkingHoursLeft > 0:      # personnel available
            self.state = True               # unit works again
            self.sensor.display = True      # sensor shows correct state
            self.sensor.state = True        # sensor works again
            
            
    def perfDistribution(self, perfIfTrue = inp['perfIfTrue'],
                         perfIfFalse = inp['perfIfFalse']):
        '''Get the performance depending on unit state. An example is if the
        Unit is working than a 80% reduction is observabel, if the unit fails
        no reduction exists.'''
        
        if self.state == True:              # if unit working
            self.performance = perfIfTrue   # reduction of pollutant high
        elif self.state == False:           # if unit failed
            self.performance = perfIfFalse  # reduction low
         
         
                
class Sensor(object):
    '''Makes a class named Sensor that is an object'''
    
    def __init__(self, display = True):
        '''initial settings for the class Sensor'''
        
        self.state = True                   # the true state of the sensor
        self.display = True                 # unit state that sensor displays
        self.measurement = self.perfDistribution(True) 
                                            # meta info about measurement
        self.sensorAge = 0                  # age of sensor
        self.failureCount = 0
        
    def update(self, failureRateSensor, unitState):
        '''Checks depending on the failure rate whether a sensor is still
        working properly. And define what kind of failure it is. '''

        if inp['randomness'] == 'no':
            if unitState == False:
                failure = int(failureRateSensor * 100)
                if self.failureCount >= failure:
                    self.display = False
                    self.failureCount += 1
                        
        if self.state == True:              # if the sensor is still working
            if failureRateSensor != 1.0:    # sensors installed
                self.display = unitState    # shows the correct state
            if failureRateSensor == 1.0:    # no sensors installed
                self.measurement = 'no measurement'
                                            # meta info about measurement
            elif random.random() < failureRateSensor/inp['failureInterval']:
                                            # prob that it shows failure
                self.state = False          # state is false now update
                if unitState == True:       # if unit is working
                    self.display = False    # shows error
                elif unitState == False:    # if unit not working
                    self.display = True     # assumed that it is working         
                    
                    
    def perfDistribution(self, unitState):
        '''Get distributions for the performance depending on unit state'''
        
        ## unit and sensor working, no action necessary
        if inp['sensorFailure'] == 'no':    # perfect sensor assumption
            self.display = unitState        # shows correct unit state
        
        ## no sensors are installed.
        if inp['failureRateSensor'] == 1.0: # 1.0 = no sensor installed
            self.measurement = 'no measurement'
                                            # meta info about measurement
        elif (self.display == True) and (unitState == True):
                                            # correctly shown that unit works
            self.measurement = 'correct negative'
                                            # no failure
        elif (self.display == False) and (unitState == True):
                                            # working but failure shown
            self.measurement = 'false positive'
                                            # false alarm
        elif (self.display == False) and (unitState == False):
                                            # failure shown as failure
            self.measurement = 'correct positive'
                                            # correct alarm
        elif (self.display == True) and (unitState == False):
                                            # failure without recognition
            self.measurement = 'false negative'
                                            # unobserved failure
            
        ## to capture the error if not defined correctly
        else:
            print('error in measurement definition')
  
  
              
class Personnel(object):
    '''Makes a class named Personnel that is an object'''
    
    def __init__(self):
        '''initial settings for the class Personnel'''
        
        self.nbOfInspector = inp['nbOfInspector']
                                            # inspectors total
        self.nbOfTechnician = inp['nbOfTechnician']
                                            # technicians total
        self.techWorkHrLeft = inp['nbOfTechnician'] * inp['nbOfHrPerDay']
                                            # workinghours available/timestep
        self.inspWorkHrLeft = inp['nbOfInspector'] * inp['nbOfIHrPerDay']
                                            # inspectionhours available/timest.

##############################################################################
##############################################################################
######################## ideas ###############################################
 
#class Processing(list):
#    '''Makes a class named Output that is a list'''
#    
#    def __init__(self):
#        '''inherits the properties of the class list'''
#        super(Processing, self).__init__()
##        self.result = {}
#        
#    def listtodict(self, listofdicts):
#        '''Store all dictionaries created by the function performancemodel()
#        as a dictinary of lists'''
#        result = {a: [d[a] for d in listofdicts] for a in inp.keys()} 
#        return result
#        
#    def fktname(self, liste, spectrum = range(1,10)):
#        result = [liste for i in spectrum]
#        return result
                 
        
        
#    def work(self):
#        if self.workingHoursLeft > 0:
#            self.unit.state = True
#            self.unit.sensor.display = True
#            self.unit.timeInspection = 1
#            self.workingHoursLeft -= 1
#        return self.workingHoursLeft
        
#    def unitcheck(self):
#        Sensor.repair
#        Unit.repair

#    def workinghoursleft(self, unitstate, sensordisplay, timeinspection, workinghoursleft, nbofinspection):
#        if self.workingHoursLeft > 0:
#            unitstate = True
#            sensordisplay = True
#            timeinspection = 1
#            workinghoursleft -= 1
#            nbofinspection += 1


#    def workinghours(self, nbOfInspector = inp['nbOfInspector'], 
#                     nbOfInspPerDay = inp['nbOfInspPerDay']):
#        self.workingHoursLeft = nbOfInspector * nbOfInspPerDay
#        return self.workingHoursLeft
#        
#        
#     def unittype(self):
#        return random.choice(inp['unitTypes'])
#    
#    def unittypeadd(self):
#        return random.choice(inp['unitTypesAdd'])
            