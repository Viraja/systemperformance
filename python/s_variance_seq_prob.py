# vim: set tabstop=4
# s_variance.py
#!/usr/bin/env python3
""" Function to plto system performance against robustnes for """

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 28.03.2020

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
import numpy as np
import random

from Classes import Unit
from functions import averageUnitPerformanceBayes, maintenancefunc, shedulInspect
from functions import unitPerformanceModelBayes

RUNS = 10000
TIMEMAX = 10*365
DATAPATH = '../data/'

def randomSensorAccuracy(min, max):
    ''' Draw sensor accuracy from a given distribution.
    '''
    return random.uniform(min, max)

def randomLambda(min, max):
    ''' Draw the lambda of the Weibull distribution from a distribution.
    '''
    return random.uniform(min, max)

def writefiles(data, path="../data/", filename="test"):
    ''' Writes the data into a json file in the provided folder.
    '''
    OUTFILE = path + fname + '.json'

    with open(OUTFILE, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4,
                  separators=(',', ': '))
        f.close()

if __name__ == '__main__':

    systemPerformance = []
    maintenancePerYear = []
    perceivedPerformanceUp = []
    perceivedPerformanceDown = []
    display = []
    for it in range(0, RUNS):
        _perf, _display, _pUp, _pDown, _maint = averageUnitPerformanceBayes(
            TIMEMAX, alarmThreshold=0.98,
            shape=2, scale=randomLambda(100, 200),
            TP=randomSensorAccuracy(0.7, 0.80), FN=0.25,
            TN=randomSensorAccuracy(0.70, 0.73), FP=0.3,
            # inspfunc=shedulInspect,
            inspfunc=None,
            interv=365, maintenancefunc=maintenancefunc)
        systemPerformance.append(_perf)
        maintenancePerYear.append(_maint)
        perceivedPerformanceUp.append(_pUp)
        perceivedPerformanceDown.append(_pDown)
        display.append(_display)

    variancePerf = np.var(systemPerformance)
    varianceMaint = np.var(maintenancePerYear)
    variancePerceivedUp = np.var(perceivedPerformanceUp)
    variancePerceivedDown = np.var(perceivedPerformanceDown)
    meanPerf = np.mean(systemPerformance)
    meanMaint = np.mean(maintenancePerYear)
    meanDisplay= np.mean(display)
    meanPerceivedUp = np.mean(perceivedPerformanceUp)
    meanPerceivedDown = np.mean(perceivedPerformanceDown)
    print("variance system performance: %s , maintenance: %s, perceivedUp: %s, perceivedDown: %s" % (variancePerf, varianceMaint, variancePerceivedUp, variancePerceivedDown))
    print("mean system performance: %s, maintenance per year: %s, display: %s percievedUp: %s, perceivedDown: %s" % (meanPerf, meanMaint, meanDisplay, meanPerceivedUp, meanPerceivedDown))
