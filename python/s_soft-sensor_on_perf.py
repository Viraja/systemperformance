# vim: set tabstop=4
# s_variance.py
#!/usr/bin/env python3
""" Function to plot system performance against the soft-sensor accuracy. This
plot is used for a publication."""

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 28.03.2020

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import random

# user
from Classes import Unit
from functions import averageUnitPerformance, maintenancefunc, shedulInspect
from functions import unitPerformanceModel
############

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
    matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
    plt.ion()
else:
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

# Globals
'''chose the number of runs, which represents the amount of units in a system
for the publication 10'000 units were modelled. The model runtime of the model
is rather long. Therefore, for testing new functions RUNS=100 was chosen.'''
RUNS = 10000
'''here every time step represents one day. Therefore the amount used for the
article was 10*365, which represents 10 years.'''
TIMEMAX = 10*365
DATAPATH = '../data/'

def randomSensorAccuracy(min, max):
    ''' Draw sensor accuracy from a given distribution.
    Arguments
    ---------
    None

    Keyword arguments
    -----------------
    min : float
        the minimal accuracy of the sensor which is expected. 0.5 is a 50%
        accurate sensor, which is a random sensor.
    max : float
        the maximal accuracy of the sensor which is expected. 0.99 is used for
        a close to perfect sensor.

    Returns
    -------
    float, array_like
        soft-sensor accuracy
    '''
    return random.uniform(min, max)

def randomLambda(min, max):
    ''' Draw the lambda of the Weibull distribution from a distribution.
    Arguments
    ---------
    None

    Keyword arguments
    -----------------
    min : float
        the minimal lambda.
    max : float
        the maximal lambda.

    Returns
    -------
    float, array_like
        lambda
    '''
    return random.uniform(min, max)

def writefiles(data, path="../data/", filename="test"):
    ''' Writes the data into a json file in the provided folder.
    '''
    OUTFILE = path + fname + '.json'

    with open(OUTFILE, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4,
                  separators=(',', ': '))
        f.close()

if __name__ == '__main__':
    # empty lists for the results of the modelling
    variancePerf = []
    varianceMaint = []
    varianecePerceived = []
    meanPerf = []
    meanMaint = []
    meanDisplay= []
    meanPerceived = []
    maintNoSensor = []
    perfNoSensors = []

    # format of the figure
    FMT = {'va':'center', 'ha':'center', 'fontsize':8, 'color':"black"}

    # soft-sensor accuracy for which the performance is modelled
    accuracy = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99]

    for isoft in range(0, len(accuracy)):
        # lists reset to an empty for every accuracy from the dictionary
        softs_accuracy = accuracy[isoft]
        systemPerformance = []
        maintenancePerYear = []
        perceivedPerformance = []
        display = []

        # a run represents an individual on-site wastewater treatment unit
        for it in range(0, RUNS):
            _perf, _perceived, _display, _maint = averageUnitPerformance(
                TIMEMAX, alarmThreshold=4,
                shape=2, scale=randomLambda(400, 2200),
                probTrue=randomSensorAccuracy(softs_accuracy, softs_accuracy),
                probFalse=randomSensorAccuracy(softs_accuracy, softs_accuracy),
                # inspfunc=shedulInspect,
                inspfunc=None,
                interv=365, maintenancefunc=maintenancefunc)
            systemPerformance.append(_perf)
            maintenancePerYear.append(_maint)
            perceivedPerformance.append(1-_perceived)
            display.append(_display)

        variancePerf.append(np.var(systemPerformance))
        varianceMaint.append(np.var(maintenancePerYear))
        varianecePerceived.append(np.var(perceivedPerformance))
        meanPerf.append(np.mean(systemPerformance))
        meanMaint.append(np.mean(maintenancePerYear))
        meanDisplay.append(np.mean(display))
        meanPerceived.append(np.mean(perceivedPerformance))

        systemPerformance = []
        maintenancePerYear = []
        perceivedPerformance = []
        display = []

    '''inspection intervall. If inspectionintervall is larger than the global
    TIMEMAX, no inspection will take place. For the article 1'000'000 was
    chosen. Here sheduled inspections are modelled as a benchmark for the
    demand-driven inspections.
    '''
    inspinterv = [1000000, 365, 121]
    for iz in range(0, len(inspinterv)):
        systemPerformance = []
        maintenancePerYear = []
        perceivedPerformance = []
        display = []

        for it in range(0, RUNS):
            _perf, _perceived, _display, _maint = averageUnitPerformance(
                TIMEMAX, alarmThreshold=4,
                shape=2, scale=randomLambda(400, 2200),
                inspfunc=shedulInspect,
                interv=inspinterv[iz], maintenancefunc=None)
            systemPerformance.append(_perf)
            maintenancePerYear.append(_maint)
            perceivedPerformance.append(1-_perceived)
            display.append(_display)

        maintNoSensor.append(np.mean(maintenancePerYear))
        perfNoSensors.append(np.mean(systemPerformance))

    # plot true performance
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(3.54, 3.54))
    ax.plot([i * 100 for i in accuracy], [i * 100 for i in meanPerf],
            marker='o', markersize=12, c='#41b6c4',
            alpha=1, markeredgecolor='#41b6c4', markeredgewidth=2,
            label='true performance', linestyle='--')
    for isoft in range(0, len(accuracy)):
        ax.annotate('SN={:.0f}'.format(meanMaint[isoft]), (accuracy[isoft] * 100,
                    (meanPerf[isoft] + 0.05) * 100),
                    **FMT)
    ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    ax.xaxis.set_major_formatter(mtick.PercentFormatter())
    ax.set_ylim(0.0, 110)
    ax.set_xlim(45, 104)

    # plot observed performance
    ax.plot([i * 100 for i in accuracy], [i * 100 for i in meanDisplay],
            marker='o', markersize=4, c='#253494',
            alpha=1, markeredgecolor='#253494', markeredgewidth=2,
            label='observed performance', linestyle='--')

    # plot horizonta lines for scheduled inspections without soft-sensors
    for it in range(0, len(perfNoSensors)):
        ax.axhline(perfNoSensors[it] * 100, linestyle='--', alpha=0.4)
        ax.annotate('N={:.0f}'.format(maintNoSensor[it]), (99,
                    perfNoSensors[it] * 100 + 2.5),
                    **FMT)
    # a vertical line marking the 83% sensor which was developped in this
    # project: https://gitlab.com/sbrml/beyondsignalquality
    # ax.axvline(83, linestyle='--', alpha=0.4)
    ax.axvspan(82.5, 85.5, color='gray', alpha=0.4, lw=0)

    ax.set(xlabel='soft-sensor accuracy', ylabel='treatment performance')
    ax.legend(loc=3)
    fig.tight_layout(pad=0.1)
    plt.show()

    print("variance system performance: %s , maintenance: %s, perceived: %s" % (variancePerf, varianceMaint, varianecePerceived))
    print("mean system performance: %s, maintenance per year: %s, display: %s percieved: %s" % (meanPerf, meanMaint, meanDisplay, meanPerceived))
    print("performanceNoSensors: %s, maintenanceNoSensors: %s, inspectionintervals: %s" % (perfNoSensors, maintNoSensor, inspinterv))
