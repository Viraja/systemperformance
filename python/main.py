# vim: set tabstop=4
# main.py
#!/usr/bin/env python3
""" Main function to run the code Hug and Maurer created 2011

to do: Check all parameters in the file inputparameter including the path to
the working folder"""

# Copyright (C) 2015 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 16.06.2015

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
import json
import matplotlib

from inputdict import inputdata as inp
from functions import pareto, variation
#from Classes import listtodict

matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'Arial'
matplotlib.rcParams.update({'font.size': 12})
#
# ## ------------- 1. Input Parameters -----------------
# ## ---------------------------------------------------
# ''' see inputdict.py file and do all changes there'''
#
# ## ------------ 2. Model ----------------------------
# ## --------------------------------------------------
#
# ''' indicate that the program is running.'''
# print('running...')
#
#
# ''' The main call to calculate a variation of a centain parameter on the input
# dictionary: creates a dictionary with a nbofvariation steps between min and
# max of the value variation(min, max, nbOfSteps, value to vary) '''

MPM = pareto(0.0, 1, inp['nbofvariations'], 'failureRateSensor', 50, 3650,
             inp['nbofvariations'], 'scale_fail')

# MPM = variation(50, 3650, 15, 'scale_fail')

OUTFILE = '../data/mpm.json'

with open(OUTFILE, 'w', encoding='utf-8') as f:
    json.dump(MPM, f, ensure_ascii=False, indent=4, separators=(',', ': '))
    f.close()
