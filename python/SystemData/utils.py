# vim: set tabstop=4
# utils.py
#!/usr/bin/env python3

# Copyright (C) 2020 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

## Imports
# 3rd party
from collections import namedtuple
from enum import Enum

# user

# ENUM_VAL = namedtuple('Results_Enum_Value', ['fmt', 'text'])
# class Result(Enum):
#     '''
#     False Positive: feature predicts condition (e.g. pH areation valley) but
#                     doesn't have condition (e.g. NH4 above threshold)
#     False Negative: feature doesn't predict condition (e.g. pH areation
#                     valley not found) but have condition (e.g. NH4 below
#                     threshold)
#     True positive: feature predicts condition (e.g. pH areation valley) and
#                    have condition (e.g. NH4 below threshold)
#     True negative: feature doens't predict condition (e.g. pH areation valley
#                    not found) and doesn't have condition (e.g. NH4 above threshold)
#     '''
#     FALSE_POSITIVE = ENUM_VAL(
#         fmt=dict(color='r', marker='v', markerfacecolor='w', markersize=8),
#         text='Expected v Got ^'
#         )
#     FALSE_NEGATIVE = ENUM_VAL(
#         fmt=dict(color='r', marker='^', markerfacecolor='w', markersize=8),
#         text='Expected ^ Got v'
#         )
#     TRUE_POSITIVE = ENUM_VAL(
#         fmt=dict(color='g', marker='v', markerfacecolor='g', markersize=8),
#         text='Expected v Got v'
#         )
#     TRUE_NEGATIVE = ENUM_VAL(
#         fmt=dict(color='g', marker='^', markerfacecolor='g', markersize=8),
#         text='Expected ^ Got ^'
#         )
#
#     @classmethod
#     def classify(cls, hasfeature, hascondition):
#         if hasfeature and hascondition:
#             return cls.TRUE_POSITIVE
#         elif (not hasfeature) and (not hascondition):
#             return cls.TRUE_NEGATIVE
#         elif hasfeature and (not hascondition):
#             return cls.FALSE_POSITIVE
#         elif (not hasfeature) and hascondition:
#             return cls.FALSE_NEGATIVE

Scenarios = namedtuple('filename', ["inp1", "minimum", "maximum", "steps", "inp2",
                                 "val2", "inp3", "val3", "inp4", "val4"])

DATAPATH = '../data/'
SCENARIO = (
    Scenarios(filename=DATAPATH+"no_sensor", inp1='scale_fail', minimum=1,
               maximum=3650, steps=15, inp2='sensorIs', val2=0, val3=None,
               inp4=None, val4=None),
    Scenarios(filename=DATAPATH+"sensor_a025", inp1='scale_fail', minimum=1,
               maximum=3650, steps=15, inp2='failureRateSensor', val2=0.25, val3=None,
               inp4=None, val4=None),
    Scenarios(filename=DATAPATH+"sensor_a050", inp1='scale_fail', minimum=1,
               maximum=3650, steps=15, inp2='failureRateSensor', val2=0.5, val3=None,
               inp4=None, val4=None),
    Scenarios(filename=DATAPATH+"sensor_a075", inp1='scale_fail', minimum=1,
               maximum=3650, steps=15, inp2='failureRateSensor', val2=0.75, val3=None,
               inp4=None, val4=None),
    Scenarios(filename=DATAPATH+"sensor_a100", inp1='scale_fail', minimum=1,
               maximum=3650, steps=15, inp2='failureRateSensor', val2=1.0, val3=None,
               inp4=None, val4=None)#,
    # Scenarios(filename=DATAPATH+"bsq80", inp1='scale_fail', minimum=1,
    #            maximum=3650, steps=15, inp2='sensorIs', val2=0, val3=None,
    #            inp4=None, val4=None)
    )
