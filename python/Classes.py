# vim: set tabstop=4
# Classes.py
#!/usr/bin/env python3
"""
This class defines the Wastewater treatment units. Giving it some
state, a failure time and attaching a sensor to it."""

# Copyright (C) 2015 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 17.06.2015

## ------------- 0. Imports and Paths ----------------
## ---------------------------------------------------
import sys

from collections import namedtuple
import random
import numpy as np
# import pdb
import math
import unittest

from inputdict import pythonScriptPath
from inputdict import inputdata as inp
#
# sys.path.append(pythonScriptPath)

## ------------- 1. Unit Class definition ------------
## ---------------------------------------------------

class Unit:
    '''Makes a class named Unit that is an object and represents all
    Wastewater treatment units that are installed.'''

    def __init__(self, state=True):
        '''Initial settings for wastewater treatment units
        This includes the step by step construction of the unit in
        the initial phase'''

        # self.timeConstructed = self.construction(inp['constructionVariation'])
        # self.unitAge = -1
        self.state = state
    #     self.performance = self.perfDistribution()
    #     self.sensor = Sensor()              # appends to every Unit a Sensor
    #     self.nbUnitsWaitingRepair = 0
    #     self.nbUnitsWaitingInsp = 0
    #     self.timeInspection = 0
    #     self.failuretime = 0
    #
    #     # containers to save results of simulation per unit
    #     self.unitPerformance = []
    #     self.systemPerfVector = []
    #     self.yworkingUnits = []
    #     self.yfailedUnits = []
    #     self.ySensorfp = []
    #     self.ySensortp = []
    #     self.ySensorfn = []
    #     self.ySensortn = []
    #     self.ySensorsf = []
    #     self.ynumberOfUnits = []
    #     self.ynbOfInspUnnecessary = []
    #     self.ynbOfInspectionTot = []
    #     self.ynbOfRepair = []
    #     self.ytest = []
    #
    # def construction(self, timeSlot=inp['constructionVariation']):
    #     '''Determines when the treatment unit was constructed. If the value
    #     in the inputparameter file is set to 0 than all unit where constructed
    #     at t = 0'''
    #     if timeSlot < 365:                  # in the first year no Variation
    #         result1 = 1                     # constructs all units at time 1
    #     else:                               # random distribution within Time
    #         result1 = int(round(random.random() * timeSlot))
    #     return result1                      # returns the constructiontime

    # def inspect(self, unitcount, interv=365, timeInsp=0, nbOfInspTot=0, nbOfRep=0):
    #     ''' Regular inspection taking place as often as interv suggests.'''
    #     if interv == 0:  # no inspection
    #         self.personnel.inspWorkingHoursLeft = 0
    #                                         # no workinghours
    #     ## If the inspection is more frequent than 0 it counts +1
    #     ## for the time since the last inspection.
    #     if interv > 0: # inspection exists
    #         timeInsp += 1 # time since last insp.
    #         if timeInsp == interv + 1:
    #             # if personnel.workingHoursLeft > 0:
    #             # currentUnit.state = True
    #             # currentUnit.sensor.display = True
    #             nbOfInspTot += 1
    #             timeInsp = 1
    #             if self.state == False:
    #                 self.repair(unitcount, 1)
    #                 nbOfRep += 1
    #     return interv, timeInsp, nbOfInspTot, nbOfRep

    def checkState(self, timecount, *, shape=2, scale=412, drawfailure=None):
        '''Checks depending on the failure rate whether a sensor is still
        working properly. And define what kind of failure it is. '''

        if self.state == True:
            val = (shape / scale * ((timecount / scale) ** (shape - 1.0)))
                                            # weibull distribu ted hazard rate
            randomNumber = np.random.random()

            if randomNumber >= val:
                return True

        return False
    #
    # def drawfailure():
    #     pass
    #
    # def drawperformance():
    #     pass
    #
    # def repair(self, daycount, techWorkingHoursLeft):
    #     '''repaire function with repaires if personnel is still available and
    #     only on weekdays. Can be turned of in inputdict.'''
    #     weekday = daycount % 7              # daycount (it) devided by 7
    #     if ((weekday == 5) or (weekday == 6)) and (inp['weekend'] == 'on'):
    #         '''For Saturday and Sunday no work'''
    #         pass                            # nothing happens
    #     ## if no weekend and personnel availabe the unit is repaired
    #     elif techWorkingHoursLeft > 0:      # personnel available
    #         self.state = True               # unit works again
    #         self.sensor.display = True      # sensor shows correct state
    #         self.sensor.state = True        # sensor works again
    #
    # def perfDistribution(self, perfIfTrue = inp['perfIfTrue'],
    #                      perfIfFalse = inp['perfIfFalse']):
    #     '''Get the performance depending on unit state. An example is if the
    #     Unit is working than a 80% reduction is observabel, if the unit fails
    #     no reduction exists.'''
    #     if self.state == True:              # if unit working
    #         self.performance = perfIfTrue   # reduction of pollutant high
    #     elif self.state == False:           # if unit failed
    #         self.performance = perfIfFalse  # reduction low

# class Sensor(object):
#     '''Makes a class named Sensor that is an object'''
#     def __init__(self, display=True):
#         '''initial settings for the class Sensor'''
#
#         self.state = True                   # the true state of the sensor
#         self.display = True                 # unit state that sensor displays
#         self.measurement = self.perfDistribution(True)
#                                             # meta info about measurement
#         self.sensorAge = 0                  # age of sensor
#         self.failureCount = 0
#
#     def update(self, *, unitState=True, probTrue=1.0-16/55,
#                probFalse=1.0-1/51):
#         ''' Updates what the soft-sensor is displaying as a state of the
#         wastewater treatment unit. '''
#
#         # generates a random number between 0 and 1.
#         number = random.random()
#         # Probabilty that if the unit works correctly that the sensor displays
#         # that it works correctly.
#         if unitState == True:              # if the sensor is still working
#             if number < (probTrue):
#                 display = True
#             else:
#                 display = False
#
#         if unitState == False:
#             if number < (probFalse):
#                 display = False
#             else:
#                 display = True
#
#         self.display = display
#
#         return display
#
#     def perfDistribution(self, unitState):
#         '''Get distributions for the performance depending on unit state'''
#
#         ## unit and sensor working, no action necessary
#         if inp['sensorFailure'] == 'no':    # perfect sensor assumption
#             self.display = unitState        # shows correct unit state
#
#         ## no sensors are installed.
#         if inp['failureRateSensor'] == 1.0: # 1.0 = no sensor installed
#             self.measurement = 'no measurement'
#                                             # meta info about measurement
#         elif (self.display == True) and (unitState == True):
#                                             # correctly shown that unit works
#             self.measurement = 'true negative'
#                                             # no failure
#         elif (self.display == False) and (unitState == True):
#                                             # working but failure shown
#             self.measurement = 'false positive'
#                                             # false alarm
#         elif (self.display == False) and (unitState == False):
#                                             # failure shown as failure
#             self.measurement = 'true negative'
#                                             # correct alarm
#         elif (self.display == True) and (unitState == False):
#                                             # failure without recognition
#             self.measurement = 'false positive'
#                                             # unobserved failure
#
#         ## to capture the error if not defined correctly
#         else:
#             print('error in measurement definition')

# class DemoTests(unittest.TestCase):
#     # This is just the basic structure to do a unit test. Not yet applied on
#     # the classes here.
#     def test_boolean(self):
#         """tests start with'test'"""
#         self.assertTrue(True)
#         self.assertFalse(False)
#     def test_add(self):
#         """docstring can be printed"""
#         self.assertEqual(2+1, 3)
#
# if __name__ == '__main__':
#     unittest.main()
